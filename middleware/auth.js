const JWTSecret = process.env.JWT_KEY;
const JWT = require('jsonwebtoken');
const User = require('../model/User');
const { authorized } = require('../helper/http_respone');

exports.admin = async (req, res, next) => {
  try {
    const headerAuth = req.headers.authorization;

    if (!headerAuth) {
      return authorized(res, 'please provide a token');
    }
    const token = headerAuth.split(' ')[1]; // mengambil token

    const decode = JWT.verify(token, JWTSecret);
    req.id = decode.id;
    req.role = decode.role; // decode payloadr jwt

    const find = await User.findById({ _id: req.id });

    const { role } = req;
    if (role !== 'admin' || !find) {
      return authorized(res, 'please provide user token');
    }
    next();
  } catch (error) {
    authorized(res, 'please provide valid token');
  }
};

exports.user = async (req, res, next) => {
  try {
    const headerAuth = req.headers.authorization;

    if (!headerAuth) {
      return authorized(res, 'please provide a token');
    }
    const token = headerAuth.split(' ')[1]; // mengambil token

    const decode = JWT.verify(token, JWTSecret);
    req.id = decode.id;
    req.role = decode.role;
    // decode payloadr jwt
    console.log(decode);

    const find = await User.findById({ _id: req.id });

    const { role } = req;
    if (role !== 'user' || !find) {
      return authorized(res, 'please provide admin token');
    }
    next();
  } catch (error) {
    return authorized(res, 'please provide valid token');
  }
};
