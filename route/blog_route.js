const cloudinary = require('cloudinary');
const Blog = require('../model/Blog');
const { responeOkData } = require('../helper/http_respone');

exports.createBlog = async (req, res, next) => {
  try {
    const { type } = req.body;
    const { user } = req;
    const findtype = await Blog.findOne({ type: type });
    if (!findtype) {
      res.status(401).json({ Massage: 'Please Input type correct' });
    }
    const { tittle, contents } = req.body;
    const blog = await new Blog({
      tittle: tittle,
      contents: contents,
      type: type,
      user: user,
    }).save();
    responeOkData(res, 'Blog successfully created', blog);
  } catch (error) {
    next(error);
  }
};

exports.updateBlog = async (req, res, next) => {
  try {
    const find = await Blog.findById({ _id: req.query.id });

    if (!find) {
      // check id blog ada apa nggak
      return res.status(404).json({ massage: 'Blog Not Found!' });
    }

    if (find.user !== req.id) {
      // check user sama apa tidak dengan decode user
      res.status(401).json({ massage: 'user not have acces' });
    }
    const upblog = await Blog.findOneAndUpdate({ _id: req.query.id }, req.body, {
      new: true,
    });
    responeOkData(res, 'blog successfully updated', upblog);
  } catch (error) {
    next(error);
  }
};

exports.deleteBlog = async (req, res, next) => {
  try {
    const find = await Blog.findById({ _id: req.query.id });
    if (!find) {
      // check id blog ada apa nggak
      return res.status(404).json({ massage: 'Blog Not Found!' });
    }

    if (find.user !== req.id) {
      // check user sama apa tidak dengan decode user
      res.status(401).json({ massage: 'user not have acces' });
    }

    const delblog = await Blog.findOneAndDelete({
      _id: req.query.id,
    });
    responeOkData(res, 'Blog successfully deleted', delblog);
  } catch (error) {
    next(error);
  }
};
exports.findBlog = async (req, res, next) => {
  try {
    const blog = await Blog.find({}).populate('type').populate('user');
    responeOkData(res, 'data foundded', blog);
  } catch (error) {
    next(error);
  }
};
exports.findMyBlog = async (req, res, next) => {
  try {
    responeOkData(res, 'blog', await Blog.find({ user: req.id }).populate('user').populate('type'));
  } catch (error) {
    next(error);
  }
};

exports.uploadimg = async (req, res, next) => {
  try {
    const data = { image: req.body.image };

    cloudinary.uploader.upload(data.image).then((result) => {
      res.status(200).send({ message: 'success', result });
    });
  } catch (error) {
    next(error);
  }
};
