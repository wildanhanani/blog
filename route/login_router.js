const bcrypt = require('bcrypt');
const JWT = require('jsonwebtoken');
const User = require('../model/User');

const JWTSecret = process.env.JWT_KEY;
const { responeOkData, dataNotfound, authorized } = require('../helper/http_respone');

exports.login = async (req, res, next) => {
  try {
    const { username, password } = req.body;
    const finduser = await User.findOne({ username: username });
    if (!username) {
      return dataNotfound(res, 'username not found');
    }
    const comparePassword = bcrypt.compareSync(password, finduser.password);
    if (!comparePassword) {
      return authorized(res, 'password not match');
    }
    const token = JWT.sign({ _id: finduser._id, role: finduser.role }, JWTSecret, { expiresIn: '24h' });
    responeOkData(res, 'Login successfully', token);
  } catch (error) {
    next(error);
  }
};
