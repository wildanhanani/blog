const bcrypt = require('bcrypt');
const User = require('../model/User');
const { responeOkData, authorized } = require('../helper/http_respone');

exports.createuser = async (req, res, next) => {
  try {
    const { username, password } = req.body;
    const passwordHash = bcrypt.hashSync(password, 10);
    const find = await User.findOne({ username: username });
    if (find) {
      return authorized(res, 'username already exist');
    }
    const user = await new User({
      username: username,
      password: passwordHash,
      role: 'user',
    }).save();
    responeOkData(res, 'user successfully created', user);
  } catch (error) {
    next(error);
  }
};
