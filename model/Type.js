const mongoose = require('mongoose');

const Typeschema = mongoose.Schema({
  type: { type: String, required: true },
});

module.exports = mongoose.model('Type', Typeschema);
