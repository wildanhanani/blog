const mongoose = require('mongoose');

const Blogschema = mongoose.Schema({
  tittle: { type: String, required: true, lowecase: true },
  contents: { type: String, required: true },
  type: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Type' },
  user: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
});

module.exports = mongoose.model('Blog', Blogschema);
